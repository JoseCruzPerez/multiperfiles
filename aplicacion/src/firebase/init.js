import firebase from 'firebase'


    var firebaseConfig = {
        apiKey: "AIzaSyAhsjR9-px1gWb8Z-JMxmoMxua_XzGfi-E",
        authDomain: "usuarios-6e780.firebaseapp.com",
        databaseURL: "https://usuarios-6e780.firebaseio.com",
        projectId: "usuarios-6e780",
        storageBucket: "usuarios-6e780.appspot.com",
        messagingSenderId: "1077774997009",
        appId: "1:1077774997009:web:1def5113abc7a12a9483b0",
        measurementId: "G-J9JNL2SJCR"
    };  

    const firebaseApp = firebase.initializeApp(firebaseConfig);
    firebaseApp.firestore().settings({timestampsInSnapshots: true });

    export default firebaseApp.firestore();